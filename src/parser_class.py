from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):
    def error(self, message):
        pass

    def handle_starttag(self, tag, attrs):
        for attr in attrs:
            if ('id' in attr[0]) & ('table_' in attr[1]) & (not "button" in attr[1]):
                print(attr)

    def handle_data(self, data):
        if data.strip():
            print(data)

    def compose_list(self, data):
        pass
