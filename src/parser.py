from html.parser import HTMLParser
from bs4 import BeautifulSoup


def read_file_as_list(name):
    import io
    with io.open(name, encoding='utf-8') as file:
        return file.readlines()


def read_file_as_string(name):
    import io
    with io.open(name, encoding='utf-8') as file:
        return file.read()


def lines(soup):
    return str(soup).split("\n")


file_string = read_file_as_string("/project/envisionparser/src/docs/VodCategoryList")
soup = BeautifulSoup(file_string, features="html.parser")


def first_occured_line_number(msg, where):
    last_occurence = -1
    for num, line in enumerate(where, 1):
        if msg in line:
            last_occurence = num
            return last_occurence
    return last_occurence


nesting_level = 0
interm_list = []


def get_data(soup, file_string):
    request = {}
    response = {}
    output_params_line_num = first_occured_line_number("Output Parameters", lines(soup))

    def parse_table(table_soup):

        second_line_num = first_occured_line_number(lines(table_soup.find_all("tr"))[1],
                                                    lines(soup))

        def print_json(table_soup):
            global nesting_level, interm_list
            result_dict = {}
            primitives = ["string", "int", "string[]", "integer[]"]
            nesting_level = 0
            interm_list = []

            def put_into_struct(tr, tds):
                name = tds[0].text.strip()
                type = tds[2].text.strip()

                def convert_type(type):
                    if '[]' in type:
                        if ('integer' in type) | ("long" in type):
                            return ["string"]
                        elif 'string' in type:
                            return ["string"]
                        else:
                            return []
                    elif ('integer' in type) | ("long" in type):
                        return "string"
                    else:
                        return type

                def build_interm_list(tr, tds):
                    global nesting_level, interm_list
                    table_id = tr.get('id')
                    # id = str(''.join(filter(str.isdigit, table_id)))
                    id = table_id
                    nest = len(id)
                    new_node = {}
                    new_node['name'] = name
                    new_node['type'] = convert_type(type)
                    new_node["id"] = id
                    new_node["parent"] = None
                    interm_list.append(new_node)

                build_interm_list(tr, td_all)

            def find_children(l: list):
                for parent in l:
                    parent["children"] = []
                    p_id = parent["id"]
                    for node in l:
                        if (node['parent'] == p_id) & (p_id is not None):
                            if node['id'] not in parent["children"]:
                                parent["children"].append(node['id'])
                return l

            def find_parents(l: list):
                for child in l:
                    for node in l:
                        if node != child:
                            if child["parent"] is None:
                                if (child["id"] > node["id"]) & (node["id"] in child["id"]):
                                    if len(child["id"].split(node["id"])[1].split("_")) < 3:
                                        if node['type'] not in primitives:
                                            child["parent"] = node["id"]
                return l

            tr_all = table_soup.find_all("tr")
            for tr in tr_all:
                td_all = tr.find_all("td")
                if len(td_all) > 0:
                    put_into_struct(tr, td_all)

            parented_list = find_parents(interm_list)
            prepared_list = find_children(parented_list)

            def create_dict(res, r):
                def get_from_l(id, l: list):
                    result = {}
                    for i in l:
                        if i['id'] == id:
                            return i
                    return result

                def parse_children(chren, dic):
                    for c_id in chren:
                        c = get_from_l(c_id, r)
                        name = c['name']
                        type = c['type']
                        children = c['children']
                        if len(children) == 0:
                            dic[name] = type
                        else:
                            if isinstance(type, list):
                                dic[name] = [{}]
                                parse_children(children, dic[name][0])
                            else:
                                dic[name] = {}
                                parse_children(children, dic[name])

                for i in r:
                    name = i['name']
                    type = i['type']
                    children = i['children']

                    if i['parent'] is None:
                        if len(children) == 0:
                            res[name] = type
                        else:
                            if isinstance(type, list):
                                res[name] = [{}]
                                parse_children(children, res[name][0])
                            else:
                                res[name] = {}
                                parse_children(children, res[name])

                return res

            res = create_dict({}, prepared_list)

            print(res)
            print("\n\n\n")

        def parse_request(table_soup):
            print_json(table_soup)

        def parse_response(table_soup):
            print_json(table_soup)

        if second_line_num < output_params_line_num:
            parse_response(table_soup)
        else:
            parse_request(table_soup)

    for table in soup.find_all('table'):
        if "Mandatory (M) or Optional (O)" in str(table):
            parse_table(table)


get_data(soup, file_string)

