from nested_lookup import nested_lookup, nested_update, nested_delete

d = [{'profile': 'Profile', 'id': 'table_0_0', 'parent': None},
     {'ID': 'string', 'id': 'table_0_0_0', 'parent': None},
     {'name': 'string', 'id': 'table_0_0_1', 'parent': None},
     {'profileType': 'integer', 'id': 'table_0_0_2', 'parent': None},
     {'familyRole': 'string', 'id': 'table_0_0_3', 'parent': None},
     {'introduce': 'string', 'id': 'table_0_0_4', 'parent': None},
     {'password': 'string', 'id': 'table_0_0_5', 'parent': None},
     {'loginAccounts': 'string[]', 'id': 'table_0_0_6', 'parent': None},
     {'quota': 'integer', 'id': 'table_0_0_7', 'parent': None},
     {'logoURL': 'string', 'id': 'table_0_0_8', 'parent': None},
     {'ratingID': 'string', 'id': 'table_0_0_9', 'parent': None},
     {'ratingName': 'string', 'id': 'table_0_0_10', 'parent': None},
     {'subjectIDs': 'string[]', 'id': 'table_0_0_11', 'parent': None},
     {'channelIDs': 'string[]', 'id': 'table_0_0_12', 'parent': None},
     {'VASIDs': 'string[]', 'id': 'table_0_0_13', 'parent': None},
     {'isShowMessage': 'integer', 'id': 'table_0_0_14', 'parent': None},
     {'templateName': 'string', 'id': 'table_0_0_15', 'parent': None},
     {'lang': 'string', 'id': 'table_0_0_16', 'parent': None},
     {'mobilePhone': 'string', 'id': 'table_0_0_17', 'parent': None},
     {'isReceiveSMS': 'integer', 'id': 'table_0_0_18', 'parent': None},
     {'isNeedSubscribePIN': 'integer', 'id': 'table_0_0_19', 'parent': None},
     {'email': 'string', 'id': 'table_0_0_20', 'parent': None},
     {'isDisplayInfoBar': 'integer', 'id': 'table_0_0_21', 'parent': None},
     {'channelListType': 'integer', 'id': 'table_0_0_22', 'parent': None},
     {'isSendSMSForReminder': 'integer', 'id': 'table_0_0_23', 'parent': None},
     {'reminderInterval': 'integer', 'id': 'table_0_0_24', 'parent': None},
     {'leadTimeForSendReminder': 'integer', 'id': 'table_0_0_25', 'parent': None},
     {'isDefaultProfile': 'integer', 'id': 'table_0_0_26', 'parent': None},
     {'deviceID': 'string', 'id': 'table_0_0_27', 'parent': None},
     {'birthday': 'string', 'id': 'table_0_0_28', 'parent': None},
     {'nationality': 'string', 'id': 'table_0_0_29', 'parent': None},
     {'receiveADType': 'string', 'id': 'table_0_0_30', 'parent': None},
     {'profilePINEnable': 'integer', 'id': 'table_0_0_31', 'parent': None},
     {'multiscreenEnable': 'integer', 'id': 'table_0_0_32', 'parent': None},
     {'purchaseEnable': 'integer', 'id': 'table_0_0_33', 'parent': None},
     {'loginName': 'string', 'id': 'table_0_0_34', 'parent': None},
     {'isOnline': 'integer', 'id': 'table_0_0_35', 'parent': None},
     {'subscriberID': 'string', 'id': 'table_0_0_36', 'parent': None},
     {'location': 'string', 'id': 'table_0_0_37', 'parent': None},
     {'sign': 'string', 'id': 'table_0_0_38', 'parent': None},
     {'createTime': 'long', 'id': 'table_0_0_39', 'parent': None},
     {'isFilterLevel': 'integer', 'id': 'table_0_0_40', 'parent': None},
     {'hasCollectUserPreference': 'integer', 'id': 'table_0_0_41', 'parent': None},
     {'pushStatus': 'integer', 'id': 'table_0_0_42', 'parent': None},
     {'profileVersion': 'long', 'id': 'table_0_0_43', 'parent': None},
     {'customFields': 'NamedParameter[]', 'id': 'table_0_0_44', 'parent': None},
     {'key': 'string', 'id': 'table_0_0_44_0', 'parent': None},
     {'values': 'string[]', 'id': 'table_0_0_44_1', 'parent': None},
     {'extensionFields': 'NamedParameter[]', 'id': 'table_0_0_45', 'parent': None},
     {'key': 'string', 'id': 'table_0_0_45_0', 'parent': None},
     {'values': 'string[]', 'id': 'table_0_0_45_1', 'parent': None},
     {'verifyType': 'integer', 'id': 'table_0_1', 'parent': None},
     {'correlator': 'string', 'id': 'table_0_2', 'parent': None},
     {'extensionFields': 'NamedParameter[]', 'id': 'table_0_3', 'parent': None},
     {'key': 'string', 'id': 'table_0_3_0', 'parent': None},
     {'values': 'string[]', 'id': 'table_0_3_1', 'parent': None}]


def find_parents(d: dict):
    for child in d:
        for node in d:
            if node != child:
                if child["parent"] is None:
                    if (child["id"] > node["id"]) & (node["id"] in child["id"]):
                        if len(child["id"].split(node["id"])[1].split("_")) < 3:
                            child["parent"] = node["id"]
    return d


# def has_keys(dic: dict):
#     return len(list(put_cells[-1].keys())) != 0
#
# def has_values(dic: dict):
#     return len(list(put_cells[-1].values())) != 0

l = [{'name': 'profile', 'type': 'Profile', 'id': 'table_0_0', 'parent': None}, {'name': 'ID', 'type': 'string', 'id': 'table_0_0_0', 'parent': 'table_0_0'}, {'name': 'name', 'type': 'string', 'id': 'table_0_0_1', 'parent': 'table_0_0'}, {'name': 'profileType', 'type': 'integer', 'id': 'table_0_0_2', 'parent': 'table_0_0'}, {'name': 'familyRole', 'type': 'string', 'id': 'table_0_0_3', 'parent': 'table_0_0'}, {'name': 'introduce', 'type': 'string', 'id': 'table_0_0_4', 'parent': 'table_0_0'}, {'name': 'password', 'type': 'string', 'id': 'table_0_0_5', 'parent': 'table_0_0'}, {'name': 'loginAccounts', 'type': 'string[]', 'id': 'table_0_0_6', 'parent': 'table_0_0'}, {'name': 'quota', 'type': 'integer', 'id': 'table_0_0_7', 'parent': 'table_0_0'}, {'name': 'logoURL', 'type': 'string', 'id': 'table_0_0_8', 'parent': 'table_0_0'}, {'name': 'ratingID', 'type': 'string', 'id': 'table_0_0_9', 'parent': 'table_0_0'}, {'name': 'ratingName', 'type': 'string', 'id': 'table_0_0_10', 'parent': 'table_0_0'}, {'name': 'subjectIDs', 'type': 'string[]', 'id': 'table_0_0_11', 'parent': 'table_0_0'}, {'name': 'channelIDs', 'type': 'string[]', 'id': 'table_0_0_12', 'parent': 'table_0_0'}, {'name': 'VASIDs', 'type': 'string[]', 'id': 'table_0_0_13', 'parent': 'table_0_0'}, {'name': 'isShowMessage', 'type': 'integer', 'id': 'table_0_0_14', 'parent': 'table_0_0'}, {'name': 'templateName', 'type': 'string', 'id': 'table_0_0_15', 'parent': 'table_0_0'}, {'name': 'lang', 'type': 'string', 'id': 'table_0_0_16', 'parent': 'table_0_0'}, {'name': 'mobilePhone', 'type': 'string', 'id': 'table_0_0_17', 'parent': 'table_0_0'}, {'name': 'isReceiveSMS', 'type': 'integer', 'id': 'table_0_0_18', 'parent': 'table_0_0'}, {'name': 'isNeedSubscribePIN', 'type': 'integer', 'id': 'table_0_0_19', 'parent': 'table_0_0'}, {'name': 'email', 'type': 'string', 'id': 'table_0_0_20', 'parent': 'table_0_0'}, {'name': 'isDisplayInfoBar', 'type': 'integer', 'id': 'table_0_0_21', 'parent': 'table_0_0'}, {'name': 'channelListType', 'type': 'integer', 'id': 'table_0_0_22', 'parent': 'table_0_0'}, {'name': 'isSendSMSForReminder', 'type': 'integer', 'id': 'table_0_0_23', 'parent': 'table_0_0'}, {'name': 'reminderInterval', 'type': 'integer', 'id': 'table_0_0_24', 'parent': 'table_0_0'}, {'name': 'leadTimeForSendReminder', 'type': 'integer', 'id': 'table_0_0_25', 'parent': 'table_0_0'}, {'name': 'isDefaultProfile', 'type': 'integer', 'id': 'table_0_0_26', 'parent': 'table_0_0'}, {'name': 'deviceID', 'type': 'string', 'id': 'table_0_0_27', 'parent': 'table_0_0'}, {'name': 'birthday', 'type': 'string', 'id': 'table_0_0_28', 'parent': 'table_0_0'}, {'name': 'nationality', 'type': 'string', 'id': 'table_0_0_29', 'parent': 'table_0_0'}, {'name': 'receiveADType', 'type': 'string', 'id': 'table_0_0_30', 'parent': 'table_0_0'}, {'name': 'profilePINEnable', 'type': 'integer', 'id': 'table_0_0_31', 'parent': 'table_0_0'}, {'name': 'multiscreenEnable', 'type': 'integer', 'id': 'table_0_0_32', 'parent': 'table_0_0'}, {'name': 'purchaseEnable', 'type': 'integer', 'id': 'table_0_0_33', 'parent': 'table_0_0'}, {'name': 'loginName', 'type': 'string', 'id': 'table_0_0_34', 'parent': 'table_0_0'}, {'name': 'isOnline', 'type': 'integer', 'id': 'table_0_0_35', 'parent': 'table_0_0'}, {'name': 'subscriberID', 'type': 'string', 'id': 'table_0_0_36', 'parent': 'table_0_0'}, {'name': 'location', 'type': 'string', 'id': 'table_0_0_37', 'parent': 'table_0_0'}, {'name': 'sign', 'type': 'string', 'id': 'table_0_0_38', 'parent': 'table_0_0'}, {'name': 'createTime', 'type': 'long', 'id': 'table_0_0_39', 'parent': 'table_0_0'}, {'name': 'isFilterLevel', 'type': 'integer', 'id': 'table_0_0_40', 'parent': 'table_0_0'}, {'name': 'hasCollectUserPreference', 'type': 'integer', 'id': 'table_0_0_41', 'parent': 'table_0_0'}, {'name': 'pushStatus', 'type': 'integer', 'id': 'table_0_0_42', 'parent': 'table_0_0'}, {'name': 'profileVersion', 'type': 'long', 'id': 'table_0_0_43', 'parent': 'table_0_0'}, {'name': 'customFields', 'type': 'NamedParameter[]', 'id': 'table_0_0_44', 'parent': 'table_0_0'}, {'name': 'key', 'type': 'string', 'id': 'table_0_0_44_0', 'parent': 'table_0_0_44'}, {'name': 'values', 'type': 'string[]', 'id': 'table_0_0_44_1', 'parent': 'table_0_0_44'}, {'name': 'extensionFields', 'type': 'NamedParameter[]', 'id': 'table_0_0_45', 'parent': 'table_0_0'}, {'name': 'key', 'type': 'string', 'id': 'table_0_0_45_0', 'parent': 'table_0_0_45'}, {'name': 'values', 'type': 'string[]', 'id': 'table_0_0_45_1', 'parent': 'table_0_0_45'}, {'name': 'verifyType', 'type': 'integer', 'id': 'table_0_1', 'parent': None}, {'name': 'correlator', 'type': 'string', 'id': 'table_0_2', 'parent': None}, {'name': 'extensionFields', 'type': 'NamedParameter[]', 'id': 'table_0_3', 'parent': None}, {'name': 'key', 'type': 'string', 'id': 'table_0_3_0', 'parent': 'table_0_3'}, {'name': 'values', 'type': 'string[]', 'id': 'table_0_3_1', 'parent': 'table_0_3'}]

parents = []
non_root_p = []
res = {}


# while (len(l) > 0) | (len(non_root_p) > 0):
#     for i in l:
#         if i["parent"] is None:
#             res[i["name"]] = i["type"]
#             parents.append(i)
#             l.remove(i)
#
#         for p in parents:
#             if p["id"] == i["parent"]:
#                 p_dict_value = nested_lookup(p["name"], res)[0]
#                 if not isinstance(p_dict_value, dict):
#                     del res[p["name"]]
#                     res[p["name"]] = {}
#                 res[p["name"]][i["name"]] = i["type"]
#             elif (p["id"] != i["parent"]) & (i["parent"] is not None) & (i not in non_root_p):
#                 non_root_p.append(i)
#             if i in l:
#                 l.remove(i)
#     if not len(l) > 0:
#         for p in non_root_p:
#             try:
#                 node = nested_lookup(p['name'], res)[0]
#             except Exception:
#                 cnode = node

def find_children(l: list):
    for parent in l:
        parent["children"] = []
        p_id = parent["id"]
        for node in l:
            if (node['parent'] == p_id) & (p_id is not None):
                if node['id'] not in parent["children"]:
                    parent["children"].append(node['id'])
    return l


r = find_children(l)


def get_from_l(id, l: list):
    result = {}
    for i in l:
        if i['id'] == id:
            return i
    return result

def parse_children(chren, dic):
    for c_id in chren:
        c = get_from_l(c_id, r)
        name = c['name']
        type = c['type']
        children = c['children']
        if len(children) == 0:
            dic[name] = type
        else:
            dic[name] = {}
            parse_children(children, dic[name])

for i in r:
    name = i['name']
    type = i['type']
    children = i['children']

    if i['parent'] is None:
        if len(children) == 0:
            res[name] = type
        else:
            res[name] = {}
            parse_children(children, res[name])

print(res)

